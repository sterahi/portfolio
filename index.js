'use strict';

var express = require('express')
var kraken = require('kraken-js')
var contentful = require('contentful')

var options, app

/* Declare Contentful info here */
// Default keys from a trial account. Don't be too crazy.
var spaceId = 'ejg0li55cy5g'
var accessId = 'af4b834f4897ad1cf271dcb8228cd54518453d71376f32f0c624422e0bd8fafc'

// Declare global.contentful here to prevent needing to call it in every template.
global.contentful = contentful.createClient({
  space: spaceId,
  accessToken: accessId
})

options = {
  onconfig: function (config, next) {
    next(null, config)
  }
}

app = module.exports = express()
app.use(kraken(options))
app.on('start', function () {
  console.log('Application ready to serve requests.')
  console.log('Environment: %s', app.kraken.get('env:env'))
})

'use strict';

module.exports = function BlogModel(content, listing) {
    return {
        name: 'blog',
        content: content,
        listing: listing
    };
};

'use strict'

module.exports = function AboutModel(content) {
    return {
        name: 'about',
        content: content
    }
}

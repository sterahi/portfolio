'use strict'

var BlogModel = require('../models/blog')
var marked = require('marked')
var strip = require('striptags')

var cf = global.contentful
// Delare values to pass to model:
var content, listing, slug, title
var home = []

function repAll(search, replacement) {
    return repl.replace(new RegExp(search, 'g'), replacement);
};

module.exports = function (router) {
  var model
  router.get('/', function (req, res, next) {
    home = []
    cf.getEntries({
      'content_type': 'misc',
      'include': 3
    }).then(function(entries) {
      entries.items.forEach(function (entry) {
        slug = entry.fields.slug
        title = entry.fields.entryTitle
        content = strip(marked(entry.fields.content).substring(0,500)) + '[...]'
        listing = 'list'
        home.push({content, listing, title, slug})
      })
      model = new BlogModel(home, listing)
      res.render('misc', model)
    })
  })

  router.get('/:slug'/*, global.cache.route()*/, function (req, res, next) {
    home = []
    cf.getEntries({
      'content_type': 'misc', // Update to match content_type
      'include': 3
    }).then(function(entries) {
      entries.items.forEach(function (entry) {
        if (req.url === '/'+ entry.fields.slug) {
        console.log(entry.fields.slug)
          slug = entry.fields.slug
          title = entry.fields.entryTitle
          content = marked(entry.fields.content)
          home.push({content, title, slug})
        }
      })
      model = new BlogModel(home)
      res.render('misc', model)
    })
  })
}

'use strict'

var AboutModel = require('../models/about')
var marked = require('marked')

var cf = global.contentful

var content
var home = []

var callContentful = function(callback) {
  cf.getEntries({
    'sys.id': '1pi0K46UogwwWUYCw6AS4A',
    'include': 3
  }).then(function(entries) {
    entries.items.forEach(function (entry) {
      content = marked(entry.fields.content)
      home.push({content})
    })
    callback(content)
  })
}

module.exports = function (router) {
  callContentful(function (content) {
    var model = new AboutModel(home)
    router.get('/', function (req, res, next) {
        res.render('about', model)
    })
  })
}

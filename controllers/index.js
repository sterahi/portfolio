'use strict'

var IndexModel = require('../models/index')
var marked = require('marked')

var cf = global.contentful
// Delare values to pass to model:
var content
var home = []

var callContentful = function(callback) {
  cf.getEntries({
    'sys.id': '46VP69QKtWmqu2Em8YUswE', // Update to match content_type
    'include': 3
  }).then(function(entries) {
    entries.items.forEach(function (entry) {
      content = marked(entry.fields.content)
      // ctas = entry.fields.produceCopy

      home.push({content})
    })
    // IMPORTANT:
    // Place fields in callback otherwise you can't access them for the model!
    callback(content)
  })
}

module.exports = function (router) {
  callContentful(function(content){
    var model = new IndexModel(home)
    router.get('/'/*, global.cache.route()*/, function (req, res, next) {
        res.render('index', model)
    })
  })

  /* VERY IMPORTANT: DON'T CHANGE THESE OUTSIDE OF THE ROUTE */
  router.get('/cache_reset', function (req, res) {
      console.log('CACHE CLEARED')
      global.cache.del('*', /* Function ( Error, Number deletions ) */ global.log)
      res.sendStatus(200)
  })

  router.post('/cache_reset', function (req, res) {
      global.cache.del('*', /* Function ( Error, Number deletions ) */ global.log)
      console.log('CACHE CLEARED')
      res.sendStatus(200)
  })
}

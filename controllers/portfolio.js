'use strict';

var PortfolioModel = require('../models/portfolio');


module.exports = function (router) {

    var model = new PortfolioModel();

    router.get('/', function (req, res) {
        
        
        res.render('portfolio', model);
        
        
    });

};

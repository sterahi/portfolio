'use strict';

var MiscModel = require('../models/misc');


module.exports = function (router) {

    var model = new MiscModel();

    router.get('/', function (req, res) {
        
        
        res.render('misc', model);
        
        
    });

};

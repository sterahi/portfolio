'use strict';

var GamingModel = require('../models/gaming');


module.exports = function (router) {

    var model = new GamingModel();

    router.get('/', function (req, res) {
        
        
        res.render('gaming', model);
        
        
    });

};

var gulp = require('gulp')
var concat = require('gulp-concat')
var sass = require('gulp-sass')
var imagemin = require('gulp-imagemin')
var cleanCSS = require('gulp-clean-css')

gulp.task('styles', function() {
  gulp.src('public/css/scss/build.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('main.css'))
    .pipe(gulp.dest('public/css'))
})

gulp.task('concat-js', function() {
    return gulp.src('public/js/src/*.js')
    .pipe(concat('app.js'))
    .pipe(gulp.dest('public/js'))
})

gulp.task('minify-css', function() {
  return gulp.src('public/css/scss/**/*.scss')
  .pipe(cleanCSS({debug: true}, function(details) {
    console.log(details.name + ': ' + details.stats.originalSize);
    console.log(details.name + ': ' + details.stats.minifiedSize);
  }))
  .pipe(concat('main.min.css'))
  .pipe(gulp.dest('public/css'));
})

gulp.task('imagemin', function() {
  gulp.src('public/images/**/*')
  .pipe(imagemin({
    verbose: true,
    progressive: true
  }))
  .pipe(gulp.dest('public/images/'))
})

gulp.task('default', function() {
  gulp.start('styles', 'concat-js') // Run styles when gulp starts
  gulp.watch(['public/css/scss/*.scss', 'public/css/scss/components/*.scss', 'public/js/src/*.js'], ['styles', 'concat-js'])
})
